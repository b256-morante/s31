const http = require("http");

const port = 3000;

const server = http.createServer(function(req,res){


	if(req.url == "/login"){

		res.writeHead(200, {"Content-Type" : "text/plain"});

		res.end("You are in the login page!");


	}  else {

		res.writeHead(404, {"Content-Type" : "text/plain"});

		res.end("Page Not Found!");
	}
	
})

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);